#!/bin/bash
#########################################
# cloud-init for Haiku
# Copyright, 2014-2021, Alexander von Gluck IV
# Feel free to use as per the MIT license
#########################################

PROVIDER=""
URI_PREFIX=""

function logProgress() {
	echo "cloud-init-haiku: $1" | logger
}

# metadataGetFile name, url, destination
function metadataGetFile() {
	ATTEMPTS=4
	TRY=0
	while [ $TRY -lt $ATTEMPTS ]; do
		curl -s -m 5 -f -o "$3" "http://169.254.169.254/$URI_PREFIX/$2" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			logProgress "Successfully pulled $1 from instance metadata."
			return 0
		fi;
		let TRY++
		logProgress "Attempt $TRY to obtain $1 from metadata server failed, retrying..."
	done;
	logProgress "Unable to obtain $1 from metadata server."
	return 1
}

# metadataGetString name, url
function metadataGetString() {
	ATTEMPTS=4
	TRY=0
	while [ $TRY -lt $ATTEMPTS ]; do
		RESULT=$(curl -s -m 5 -f "http://169.254.169.254/$URI_PREFIX/$2")
		if [ $? -eq 0 ]; then
			logProgress "Successfully pulled $1 from instance metadata."
			return 0
		fi;
		let TRY++
		logProgress "Attempt $TRY to obtain $1 from metadata server failed, retrying..."
	done;
	logProgress "Unable to obtain $1 from metadata server."
	return 1
}

# metadataServerDetect
function metadataServerDetect() {
	ATTEMPTS=5
	TRY=0
	while [ $TRY -lt $ATTEMPTS ]; do
		curl -s -m 3 -f "http://169.254.169.254/latest/meta-data/hostname" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			logProgress "Successfully found AWS-like metadata service."
			URI_PREFIX="latest/meta-data"
			PROVIDER="aws"
			return 0
		fi;
		curl -s -m 3 -f "http://169.254.169.254/metadata/v1/hostname" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			logProgress "Successfully found Digital Ocean-like metadata service."
			URI_PREFIX="metadata/v1"
			PROVIDER="digitalocean"
			return 0
		fi;
		curl -s -m 3 -f "http://169.254.169.254/v1/hostname" > /dev/null 2>&1
		if [ $? -eq 0 ]; then
			logProgress "Successfully found Vultr-like metadata service."
			URI_PREFIX="v1"
			PROVIDER="vultr"
			return 0
		fi;
		let TRY++
		logProgress "Waiting on metadata service..."
		sleep $TRY
	done;
	logProgress "Unable to obtain $1 from metadata server."
	return 1
}

metadataServerDetect
if [[ $? -ne 0 ]]; then
	logProgress "Unable to find metadata server for cloud-init!"
	exit 1
else
	logProgress "Found metadata server for cloud-init!"
fi

#########################################
USER_SETTINGS_DIR=$(finddir B_USER_SETTINGS_DIRECTORY)
SYST_SETTINGS_DIR=$(finddir B_SYSTEM_SETTINGS_DIRECTORY)
SSH_SETTINGS_DIR="$USER_SETTINGS_DIR/ssh"
NET_SETTINGS_DIR="$SYST_SETTINGS_DIR/network"

#################################
# Network BOOTSTRAP
#################################
logProgress "Network bootstrap..."

# Grab instance hostname and set it
metadataGetFile "instance hostname" "hostname" "$NET_SETTINGS_DIR/hostname"
if [ $? -eq 0 ]; then
	hostname $(cat $NET_SETTINGS_DIR/hostname) > /dev/null 2>&1
fi

#################################
# SSH BOOTSTRAP
#################################
logProgress "Bootstrapping ssh server..."

# check for settings directory...
if [ ! -d "$SSH_SETTINGS_DIR" ]; then
	mkdir -p "$SSH_SETTINGS_DIR"
	chmod 700 "$SSH_SETTINGS_DIR"
fi

# sshd user may not exist
id sshd &> /dev/null
if [ $? -ne 0 ]; then
	logProgress "Creating sshd user"
	useradd sshd &> /dev/null
	kill sshd
	$(which sshd)
fi;

# Fetch public key using HTTP
if [ ! -f $SSH_SETTINGS_DIR/authorized_keys ]; then
	if [ $PROVIDER == "aws" ]; then
		metadataGetFile "SSH public key" "public-keys/0/openssh-key" "$SSH_SETTINGS_DIR/authorized_keys"
		if [ $? -eq 0 ]; then
			echo "Found AWS-like SSH public keys!"
			chmod 0600 "$SSH_SETTINGS_DIR/authorized_keys"
		fi
	elif [ $PROVIDER == "digitalocean" ] || [ $PROVIDER == "vultr" ]; then
		metadataGetFile "SSH public key" "public-keys" "$SSH_SETTINGS_DIR/authorized_keys"
		if [ $? -eq 0 ]; then
			echo "Found DigitalOcean-like SSH public keys!"
			chmod 0600 "$SSH_SETTINGS_DIR/authorized_keys"
		fi
	fi
fi;

logProgress "Boootstrap process complete."
