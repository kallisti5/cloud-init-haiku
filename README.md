cloud-init for Haiku
====================

> This tool is an overly simple shell script and could
> be better rewritten in a higher level language.

This script performs metadata initialization for Haiku
via several common metadata services.

## Providers

### Working Providers

* AWS
* Digital Ocean
* Vultr

### Not-Working Providers

* GCP (unicorn http://metadata.google.internal)

## Scope

### System items provisioned

* Hostname
* SSH authorized_keys

### Future items provisioned

* Initial-boot scripts / software installations
* Public / Private network ip addresses
